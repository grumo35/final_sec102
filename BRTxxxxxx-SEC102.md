---
header-includes:
- \usepackage{xcolor}
---

<!--- 
*************************************
Original author: Bertanier J
Educational use only for CNAM SEC102
If you need to use this file for another practice :
formationforensics@gmail.com
*************************************

Install first in Linux:
pandoc
    texlive-latex-base
texlive-fonts-recommended
texlive-fonts-extra

*************************************

Before writing your analysis try to generate PDF file by use :
pandoc -s -o BRTxxxxxx-SEC102.pdf BRTxxxxxx-SEC102.md

*************************************

To generate your PDF final report : 
pandoc -s -o BRTxxxxxx-SEC102.pdf BRTxxxxxx-SEC102.md

*************************************
-->


![](./cnam.png){width=70%}

&nbsp;

\begin{center}  
{\Large CONSERVATOIRE NATIONAL DES ARTS ET MÉTIERS \\
CENTRE RÉGIONAL DE BRETAGNE \\
ANNÉE UNIVERSITAIRE 2020/ 2021}
\end{center}  

\begin{center}  
\textbf{ Code UE: SEC102}   \\
\textbf{ Menaces informatiques et codes malveillants : analyse et lutte}  
\end{center}  
&nbsp;

**1ère session**  
**Examen non surveillé distant**  
**Date : du 11 au 23 Janvier 2021**  

&nbsp;

**Votre devoir est à rendre dans le temps imparti et à déposer sur votre enseignement à l’endroit où vous avez pris connaissance de ce sujet d’examen.**  
**- Renommez votre /vos document(s) avec \textcolor{red}{votre code auditeur breton (BRT)} indiqué sur votre convocation et le code UE»Exemple : BRT425868-SEC102.md**  
**- \textcolor{red}{Convertissez votre document md au format PDF} via la commande**   
**pandoc -s -o BRTxxxxxx-SEC102.pdf BRTxxxxxx-SEC102.md**
**- Déposez votre fichier PDF à l’endroit dédié :**  
    **• Cliquez sur : « AJOUTER UN TRAVAIL »**    
    **• Glissez-déposer votre document et cliquez sur**   **« ENREGISTRER »**  
**- Assurez-vous d’avoir bien \textcolor{red}{remis l’intégralité de vos documents \underline{AVANT} d’envoyer} votre devoir.**  
**– Pour terminer, cliquez sur le bouton « ENVOYER LE DEVOIR » et \textcolor{red}{confirmez votre remise}.**  

Nom de l’enseignant :  *BERTANIER Jessie*  

<!--- 
complete with your personal informations

If you performed the exam for two peoples, add the following line:
**Code second auditeur CNAM : BRTxxxxxx-SEC102** 

-->
**Code auditeur CNAM : BRT478174-SEC102** 
**Code second auditeur CNAM :  BRT478206-SEC102** 
**Code troisieme auditeur CNAM : BRT477783-SEC102**

(A renseigner à partir de votre convocation. Votre copie doit rester anonyme)

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>
# Note

Note du dossier sur 100 points :  
Note du dossier sur 20 points :  

# Consignes

*Note importante: ne pas modifier le formamet et la structure du fichier.*  
*Les réponses peuvent être multiples et devront être séparées par une virgule*  

# Analyse mémoire RAM

## Synthèse de l'analyse

5 points  

| n°            |Question                   |                     Réponses                          |
| ------------- |:--------------------------| ------------------------------------------------------|
02-001 | *Date d'exécution du malware* | 2020-10-20 17:15:38 UTC+0000
02-002 | *Vecteur de compromssion* | téléchargement, exécution depuis explorer.exe
02-003 | *Nom d'utilisateur de la machine* | whiterose 
02-004 | *Profile d'analyse* | Win7SP1x64
02-005 | *PID(s) du processus vérolé* | 3032, 3064
02-006 | *IP du CC* | 192.168.40.3
02-007 | *PID du processus père du processus vérolé*| 2120
02-008 | *Emplacement du malware* | `C:\Users\whiterose\Downloads\`
02-009 | *Ip du serveur de téléchargement* | 192.168.40.6
02-010 | *Nombre de page RWX du malware* | 3  
  
<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>


## Méthodologie d'analyse

*Veuillez décrire les étapes de vos analyses qui vous ont permis de trouver des preuves numériques.*  
*Veuillez ne pas dépasser une page d’écriture.*  

15 points  

On commence par déterminer le profil adéquat pour l'analyse de ce dump mémoire à l'aide de la commande `./vol.py -f Windows_7_x64-Snapshot.vmem imageinfo`, plusieurs profils sont proposés, on choisit le premier **Win7SP1x64** sur lequel les analyses de Volatility vont se baser.

Ensuite on effectue des commandes de base pour faire un peu de reconnaissance sur la machine et connaitre l'environnement, on affiche les registres à l'aide de la commande `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 hivelist`, on obtient alors le nom de l'utilisateur de la machine **whiterose**.

Suite logique, on voudrait voir un arbre ordonnée en dépendances des processus en cours `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 pstree`, on remarque alors qu'étrangement que **skype.exe** en plus d'avoir deux identifiants de processus (**PIDs 3032 et 3064**) est exécuté à partir de Windows Explorer (**PID 2120**). On obtient aussi sa date d'exécution **2020-10-20 17:15:38 UTC+0000**.

Maintenant que nous avons possiblement identifié le malware, il serait intéressant de savoir où il se trouve, `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 cmdline | grep "skype"`, on obtient sa localisation : **`C:\Users\whiterose\Downloads\skype.exe`**. On confirme au passage le fait que le malware a bien deux PID. De plus le fait que ce dernier soit localisé dans le dossier Download de l'utilisateur fait penser à une compromission courante, le cas possible d'un téléchargement de malware en ligne et son exécution sur la machine `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 iehistory | grep "skype"`.
On voit alors que le fichier skype.exe a été téléchargé à l'adresse http://**192.168.40.6**/Software/Windows/Communication/skype.exe ce qui semble être le serveur de téléchargement.

Maintenant, l'attaque paraissant comme étant plutôt courante elle devrait communiquer sur le réseau, l'utilisation de netscan `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 netscan` permet d'afficher les connexions TCP ou des sockets. On peut remarquer que skype.exe se connecte à une adresse IP distante **192.168.40.3** qui n'est autre que celle du Command&Control.

Afin de pousser notre recherche sur le malware, il est possible d'utiliser le plugin malfind permettant la recherche de code malveillant `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 malfind` cependant du code potentiellement malveillant n'est situé que dans les process svchost.exe (PID 1956) et explorer.exe (PID 2120) qui cependant est le porcessus père de skype.exe. On pourrait donc penser que le malware n'a pas de pages RWX.

Il faut donc partir sur une autre piste, examiner plus en profondeur le malware `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 psinfo -p 3064`, le plugin psinfo permet d'obtenir des informations plus détaillées sur le processus 3064. Une ligne nous interpelle : *Memory Protection: PAGE_EXECUTE_WRITECOPY*, effectivement il peut s'agir d'une technique anti-debugging couramment utilisée par les malwares sous Windows 7 (elle est d'ailleurs pour de nombreux antivirus un critère de détection d'une injection de code malveillant). Ce qui est donc une bonne piste.

L'utilisation de vadinfo va nous permettre d'avoir plus de détails sur les VAD (Virtual Address Descriptor) du processus malveillant `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 vadinfo -p 3064 | grep "PAGE_EXECUTE_READWRITE"` et plus particulièrement les pages dont la protection est *PAGE_EXECUTE_READWRITE*, elles sont au nombre de **3**.

Finalement, on peut effectuer la commande dlllist sur le PID 3064 afin de vérifier les dll (Dynamic Link Library) associées au processus skype.exe (PID 3064) `./vol.py -f Windows_7_x64-Snapshot3.vmem --profile Win7SP1x64 dlllist -p 3064`, les dll chargées sont visiblement pour la plupart des modules python (modules cryptogrpahiques, de hash, de caractères unicodes), on peut donc supposer que le malware est un script python compilé au format exécutable. Cette hypothèse devra être confirmée par le biais d'un analyse plus poussée (décompilation) du malware.
<div style="page-break-after: always; visibility: hidden"> 
\pagebreak
</div>

# Analyse dump disque dur

\textcolor{red}{\textbf{\underline{Attention}} :}  
Ne pas prendre en compte les fichiers présents dans les répertoires des utilisateurs ADMIN*

## Synthèse de l'analyse

5 points  

| n°            |Question                   | Réponses                                              |
| ------------- |:--------------------------| ------------------------------------------------------|
03-001 | *sha256 du malware* |  db27fd09303a4b73f0635e779269c53938425b7f631345b5f2a96739f2185df7   
03-002 | *Que retrouve t-on comme information sur le malware dans le fichier NTUSER.DAT* | Clés MRU + historique explorer ( socket http  visibles )
03-003 | *Peut on trouver le hash du mdp du user connecté (si oui donner le hash)* | Oui - 6f50ed6a0c799d3fc3f2d593d37890ca
03-004 | *Peut-on lire les documents ?* | Non sauf les fichiers avec les URLs
03-005 | *Dans le fichier manifest du malware, l'architecture du malware y est noté. Quel est il* | x86
03-006 | *Type de partition * |  NTFS (vol.2)
03-007 | *Nombre de User ayant utilisés la machine* | 2 (admindom & whiterose)
03-008 | *Nombre de fichiers téléchargés* | 6

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>


## Méthodologie d'analyse

On commence à chercher ce qui aurait pu être télécharger ou ce qui aurait pu être branché au poste car ce sont des vecteurs classiques d'attaque.

Nous allons donc utiliser autopsy ici.
Lorsque l'on regarde dans le vol2 et dans ses users, nous pouvons voir qu'un compte a été plus utilisé que les autres : **whiterose**.
Concernant la partie USB, rien ne semble avoir été branché.

Nous allons utiliser les informations de la ruche avec la base **SAM**, **SYSTEM** et **SECURITY**, présent dans `C:/Windows/System32/config`.
Dedans, nous allons retrouver les utilisateurs s'étant connecté. Grâce à cela et secretdump.py, nous pouvons déchiffrer ce qui est noté.
Seulement deux utilisateurs se sont connectés sur ce poste : admindom et whiterose. 
Nous pouvons donc nous pencher seulement sur ces utilisateurs.

Lorsque l'on regarde dans les téléchargements de whiterose, nous pouvons voir 6 éxécutables téléchargés dont 1 ayant une spécificité : il est suivi de ":Zone.Identifier".
Cela correspond à des métadonnées d'un fichier. Rien d'alarmant, cependant c'est le seul l'ayant. A revoir.

Nous regardons maintenant le **NTUSER.DAT** de l'utilisateur "whiterose".
Nous pouvons voir que c'est skype.exe qui a été lancé le dernier grace à **"OpenSavePidlMRU"** présent dans le fichier. Cela semble confirmer que skype.exe serait le malware mais aucune certitude encore.
De ce que l'on peut voir, skype.exe est lancé depuis Download et non dans Program Files, ce n'est pas un fonctionnement légitime.

Si l'on recherche skype.exe dans le NTUSER.DAT `strings 'NTUSER.DAT' | grep -C 10 skype.exe)` et l'on peut voir qu'il y a des relations avec des certificats (Certh, Certificates, CRLs, etc.). 

Cela confirme encore une fois que skype.exe est un malware.

15 points  

<!--- uncomment and tape your response here -->

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak
</div>

# Analyse réseau

## Synthèse de l'analyse
5 points  

| n°            |Question                   | Réponses                                              |
| ------------- |:--------------------------| ------------------------------------------------------|
04-001 | *Combien(s) d'adresse(s) IP privée (hors broadcast)* |  10 
04-002 | *Ip du serveur de téléchargement du malware* | http://192.168.40.6
04-003 | *Nombre de protocol en clair* | 16
04-004 | *Quel est le nom du Domaine* | DarkArmy.org
04-005 | *Nom des fichiers utilisés par le malware* |  start,key,key.hex,uid,uid.txt
04-006 | *Adresse Ip du serveur NTP* | 192.168.40.1
04-007 | *Adresse IP du serveur de controle du malware* | 192.168.40.3
04-008 | *Quel est le nom du serveur de domaine* | darkserver
04-009 | *Fichier manquant que le malware souhaite utiliser* | stop
04-010 | *Le serveur du controle de malware ainsi que le serveur de téléchargement font il partie du domaine de la machine piratée ?* | non 

Questions bonus (5 points):  
Quels utilisateurs ainsi que leurs adresses IP sont actuellement connectés sur le réseau ?  
```
WIN-KJLSUGHFRFV.DARKARMY.ORG 	192.168.40.5	whiterose, admindom  
DESKTOP-LD31JKP.DARKARMY.ORG	192.168.40.2	elliot  
DESKTOP-V82N4MR.DARKARMY.ORG	192.168.40.4	darlene 
```

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>




## Méthodologie d'analyse

*Veuillez décrire les étapes de vos analyses qui vous ont permis de trouver des preuves numériques.*  
*Veuillez ne pas dépasser une page d’écriture.*  

Mon objectif était de répondre aux différentes questions de manière simple et efficace mais également d'en apprendre plus sur le malware et son fonctionnement, ainsi que l'environement dans lequel il évolue.

J'ai commencé par ouvrir la capture dans wireshark afin d'avoir un aperçu du contenu de celle-ci. Je fais rapidement le tour des protocoles et des adresses au vu de leurs dissemblances je décide d'éxporter la capture vers une infrastructure Elasticsearch Logstash Kibana, pour avoir une vue plus claire de la timeline et des échanges réalisés.

Pour cela j'ai dumpé les données à l'aide de `tshark -T ek -r dump.pcapng > dump.json` qui va automatiquement formater les documents pour elasticsearch, je n'ai plus qu'a les ingérer via logstash.    

Les données apparaissent alors dans l'index **packets-2020-10-20** je filtre donc les données avec une timeline du 10 octobre de 00:00 à 23h59 dans Kibana.  

L'analyse peut donc commencer, je commence par lister les protocoles évoquant des services connus ntp,kerberos,http,ldap,mdns. Puis je dresse un rapide tableau des machines et de leurs roles au vu des échanges de protocoles :
```
DARKSERVER.DARKARMY.ORG 	    192.168.40.1 	(DNS,NTP,SMB,LDAP)  Domain Controller
DESKTOP-LD31JKP.DARKARMY.ORG	192.168.40.2	("Machine Client")  
				                192.168.40.3	( HTTP Serveur ) 
DESKTOP-V82N4MR.DARKARMY.ORG	192.168.40.4	("Machine Client")  
WIN-KJLSUGHFRFV.DARKARMY.ORG 	192.168.40.5 	("Machine Client",HTTP)    
				                192.168.40.6    ( HTTP Serveur )     
```

En m'interessant au protocole HTTP **192.168.40.5** ressort comme étant à l'origine de plusieurs téléchargements vers **192.168.40.6**. En plus du téléchargement les requêtes http effectués vers **192.168.40.3** contienent des informations démontrant l'utilisation du serveur en tant que Command and Control cf annexe reseau[1].  

Au vu des échanges HTTP en annexe[1] notamment la requête numéro 2 qui montre l'utilisation d'un **uid** et d'une **clé**,on peut même déduire que le malware est probablement de type **cryptolocker**  on remarque également des reqûetes http cherchant une répponse a l'uri **/stop** le malware attend alors une réponse differente de 404 pour s'arrêter.

Après avoir vu la plupart des échanges du malware et de son serveur je cherche d'éventuels mouvements latéraux mais rien ne semble ressortir. 

Au moment de la fin de la capture le malware n'a toujours pas reçu le signe de s'arrêter.

15 points  

<!--- uncomment and tape your response here -->



<div style="page-break-after: always; visibility: hidden"> 
\pagebreak
</div>

# Analyse Statique du malware

## Synthèse de l'analyse
5 points  


| n°            |Question                   | Réponses                                              |
| ------------- |:--------------------------| ------------------------------------------------------|
04-001 | Langage de programmation utilisé pour coder le malware|  python3.7
04-002 | Date de compilation du malware |  8 Août 2020 à 14:29:54
04-003 | Combien de DLL importée| 49
04-004 | Nombre de section |  6
04-005 | Entropy du malware |  7.993184 (probably packed)
04-006 | Nombre de fonctions importées | 93
04-007 | Version du logiciel de programmation |  python3.7 
04-008 | Le logiciel supporte t'il les sockets |  Oui 
04-009 | Architecture du malware (ex :x86) | i386
04-010 | Taille du code en octet | 128000


<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>


## Méthodologie d'analyse

*Veuillez décrire les étapes de vos analyses qui vous ont permis d'analyser le malware.*  
*Veuillez ne pas dépasser une page d’écriture.*  

15 points  

Pour cette analyse statique j'ai commencé par le classique **strings** qui m'indique rapidement que le malware se base sur **python3.7** au vu des librairies urllib,socket,socketserver,blibcrypto,blibssl et des extensions .pyd

Pour la date de compilation du malware je fais appel a objdump

>objdump -x skype.exe | grep "Date"

Afin d'obtenir le nombre de sections ainsi que l'entropy du PE j'ai utilisé **pescan**

> pescan skype.exe 
```
file entropy:                    7.993184 (probably packed)
fpu anti-disassembly:            no
imagebase:                       normal
entrypoint:                      normal
DOS stub:                        normal
TLS directory:                   not found
timestamp:                       normal
section count:                   6
```
Pour les fonctions importés j'ai compté les fonctions présentes dans les DLL importés.
cf annexe[1]   

> objdump -x skype.exe | grep -i "SizeOfCode"  

`echo $((0x0001f400))` Notations des octest de hexa à décimal


<div style="page-break-after: always; visibility: hidden"> 
\pagebreak
</div>

# TimeLine and conclusion

## Timeline

1,5 points  

| n°            |Question                   | Réponses                                              |
| ------------- |:--------------------------| ------------------------------------------------------|
05-001 | *Date de dépose du malware* | Oct 20, 2020 @ 17:12:05
05-002 | *Date de la première execution du malware* |  Oct 20, 2020 @ 17:15:38
05-003 | *Sur quel type de réseau est la machine infecté* | Un réseau local

## Conclusion

3,5 points  

| n°            |Question                   | Réponses                                              |
| ------------- |:--------------------------| ------------------------------------------------------|
06-001 | *Sévérité du malware* |  9/10 (perte de données potentiellement critiques pour l'utilisateur)
06-002 | *Nombre de machine(s) infectée(s)* |  1
06-003 | *Système d'exploitation affecté* | Windows 7
06-004 | *Type de malware (eg:keylogger)* | Cryptolocker (XOR)
06-005 | *Méthode de récupération des clés* |  HTTP en clair depuis un serveur distant
06-006 | *Quel(s) répertoire(s) infecté(s)* |  Downloads, Document, Pictures, Desktop, Videos, Music
06-007 | *Méthode utlisée par le malware pour démarrer* |  Exécution par l'utilisateur (lancement de skype.exe)

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>

# Recommandations

*Veuillez décrire les recommandations que vous proposeriez*  
*Veuillez ne pas dépasser une page d'écriture.*  

15 points  

### Reduction de la surface d'attaque
- Ne pas télécharger de fichiers provenant de sources peu sûres ;
- Activer un antivirus fiable & reconnu sur sa machine;
- Bien cloisonner les utilisateurs de la machine ;
- Faire attention aux droits qui sont accordés, à quels processus et quels utilisateurs ;
- Sensibiliser les utilisateurs aux risques liés à l'utilisation d'une machine connecté au réseau (phishing, clés usb, gestion des droits & mot de passes ...);

### Remédiation

- Si jamais l'utilisateur a accès aux données de la capture réseau il sera en mesure de déchiffrer ses données avec la clé récupérée en paramètre car l'algorithme utilisé pour le chiffrement est du XOR ce qui veut dire qu'il est reversible.

- L'utilisation de sauvegarde pour restituer les données et limiter l'impact (si possible stocker sur un autre réseau).

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>


# Annexes
<!--
Si vous souhaitez utiliser des images :  
<div align="center">
![](chemin de l'image sur votre machine)
</div>

Si vous souhaitez ajouter du code de programmation :  

```
If you need copy and paste code
Enter code here
Else remove line in back quotes
```
-->


## Analyse de la mémoire vive


- `root@lab: volatility --profile=Win7SP1x64 -f dumpram.vmem vadinfo -p 3064 | grep "PAGE_EXECUTE_READWRITE"`
```
Volatility Foundation Volatility Framework 2.6.1
Protection: PAGE_EXECUTE_READWRITE
Protection: PAGE_EXECUTE_READWRITE
Protection: PAGE_EXECUTE_READWRITE
```
- psinfo
```
Process Information:
	Process: skype.exe PID: 3064
	Parent Process: skype.exe PPID: 3032
	Creation Time: 2020-10-20 17:15:38 UTC+0000
	Process Base Name(PEB): skype.exe
	Command Line(PEB): "C:\Users\whiterose\Downloads\skype.exe" 

VAD and PEB Comparison:
	Base Address(VAD): 0x0
	Process Path(VAD): NA
	Vad Protection: NA
	Vad Tag: NA

	Base Address(PEB): 0x3a0000
	Process Path(PEB): C:\Users\whiterose\Downloads\skype.exe
	Memory Protection: PAGE_EXECUTE_WRITECOPY
	Memory Tag: Vad 

	No Hexdump: Memory Unreadable at 0x003a0000


Similar Processes:
C:\Users\whiterose\Downloads\skype.exe
	skype.exe(3064) Parent:skype.exe(3032) Start:2020-10-20 17:15:38 UTC+0000
No PEB
	skype.exe(3032) Parent:explorer.exe(2120) Start:2020-10-20 17:15:38 UTC+0000

Suspicious Memory Regions:
---------------------------------------------------

```
## Analyse de disque
## Analyse réseau

### Requêtes HTTP vers 192.168.40.3

- `Oct 20, 2020 @ 17:15:43.109 GET http://192.168.40.3/start`  
```http
HTTP/1.0 200 OK
``` 
- `Oct 20, 2020 @ 17:21:56.318 GET http://192.168.40.3/?`  
`uid=56831da9-2f7f-433d-aa31-be7149ddc795& `
`key=993753902beabc7b2c0abadccb7ae0f2f358a946c9ad8e4b1c623da8d2c8a478`  
```html
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /?uid=56831da9-2f7f-433d-aa31-be7149ddc795&amp;key=993753902beabc7b2c0abadccb7ae0f2f358a946c9ad8e4b1c623da8d2c8a478</title>
</head>
<body>
<h1>Directory listing for /?uid=56831da9-2f7f-433d-aa31-be7149ddc795&amp;key=993753902beabc7b2c0abadccb7ae0f2f358a946c9ad8e4b1c623da8d2c8a478</h1>
<hr>
<ul>
<li><a href="key">key</a></li>
<li><a href="key.hex">key.hex</a></li>
<li><a href="start">start</a></li>
<li><a href="uid.txt">uid.txt</a></li>
</ul>
<hr>
</body>
</html>
```
- `Oct 20, 2020 @ 17:22:00.328 GET http://192.168.40.3/stop`   
- `Oct 20, 2020 @ 17:22:04.338 GET http://192.168.40.3/stop`  
- `Oct 20, 2020 @ 17:22:08.346 GET http://192.168.40.3/stop`  
- `Oct 20, 2020 @ 17:22:12.355 GET http://192.168.40.3/stop`  
- `Oct 20, 2020 @ 17:22:16.364 GET http://192.168.40.3/stop`  
- `Oct 20, 2020 @ 17:22:20.373 GET http://192.168.40.3/stop`
```html
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <title>Error response</title>
    </head>
    <body>
        <h1>Error response</h1>
        <p>Error code: 404</p>
        <p>Message: File not found.</p>
        <p>Error code explanation: HTTPStatus.NOT_FOUND - Nothing matches the given URI.</p>
    </body>
</html>

```




## Analyse du malware

```
DLL Name: KERNEL32.dll
	vma:  Hint/Ord Member-Name Bound-To
	2bc74	  611  GetModuleFileNameW
	2bc8a	  669  GetProcAddress
	2bc9c	  457  GetCommandLineW
	2bcae	  553  GetEnvironmentVariableW
	2bcc8	 1262  SetEnvironmentVariableW
	2bce2	  341  ExpandEnvironmentStringsW
	2bcfe	  739  GetTempPathW
	2bd0e	 1451  WaitForSingleObject
	2bc60	 1256  SetDllDirectoryW
	2bd2c	  556  GetExitCodeProcess
	2bd42	  219  CreateProcessW
	2bd54	  702  GetStartupInfoW
	2bd66	  935  LoadLibraryExW
	2bd78	  178  CreateDirectoryW
	2bd8c	  410  FormatMessageW
	2bd9e	  933  LoadLibraryA
	2bdae	  977  MultiByteToWideChar
	2bdc4	 1485  WideCharToMultiByte
	2bd24	 1362  Sleep
	2bc50	  592  GetLastError
	2c364	  254  DecodePointer
	2be3a	 1410  UnhandledExceptionFilter
	2be56	 1347  SetUnhandledExceptionFilter
	2be74	  521  GetCurrentProcess
	2be88	 1377  TerminateProcess
	2be9c	  877  IsProcessorFeaturePresent
	2beb8	 1069  QueryPerformanceCounter
	2bed2	  522  GetCurrentProcessId
	2bee8	  526  GetCurrentThreadId
	2befe	  726  GetSystemTimeAsFileTime
	2bf18	  843  InitializeSListHead
	2bf2e	  871  IsDebuggerPresent
	2bf42	  615  GetModuleHandleW
	2bf56	 1197  RtlUnwind
	2bf62	 1291  SetLastError
	2bf72	  293  EnterCriticalSection
	2bf8a	  930  LeaveCriticalSection
	2bfa2	  261  DeleteCriticalSection
	2bfba	  840  InitializeCriticalSectionAndSpinCount
	2bfe2	 1395  TlsAlloc
	2bfee	 1397  TlsGetValue
	2bffc	 1398  TlsSetValue
	2c00a	 1396  TlsFree
	2c014	  414  FreeLibrary
	2c022	  456  GetCommandLineA
	2c034	 1104  ReadFile
	2c040	  194  CreateFileW
	2c04e	  543  GetDriveTypeW
	2c05e	  574  GetFileType
	2c06c	  127  CloseHandle
	2c07a	 1027  PeekNamedPipe
	2c08a	 1374  SystemTimeToTzSpecificLocalTime
	2c0ac	  349  FileTimeToSystemTime
	2c0c4	  585  GetFullPathNameW
	2c0d8	  582  GetFullPathNameA
	2c0ec	 1173  RemoveDirectoryW
	2c100	  360  FindClose
	2c10c	  366  FindFirstFileExW
	2c120	  383  FindNextFileW
	2c130	 1314  SetStdHandle
	2c140	 1219  SetConsoleCtrlHandler
	2c158	  266  DeleteFileW
	2c166	  704  GetStdHandle
	2c176	 1505  WriteFile
	2c182	  337  ExitProcess
	2c190	  614  GetModuleHandleExW
	2c1a6	  420  GetACP
	2c1b0	  819  HeapFree
	2c1bc	  815  HeapAlloc
	2c1c8	  494  GetConsoleMode
	2c1da	 1102  ReadConsoleW
	2c1ea	 1277  SetFilePointerEx
	2c1fe	  476  GetConsoleCP
	2c20e	  147  CompareStringW
	2c220	  918  LCMapStringW
	2c230	  515  GetCurrentDirectoryW
	2c248	  402  FlushFileBuffers
	2c25c	 1261  SetEnvironmentVariableA
	2c276	  562  GetFileAttributesExW
	2c28e	  882  IsValidCodePage
	2c2a0	  646  GetOEMCP
	2c2ac	  435  GetCPInfo
	2c2b8	  551  GetEnvironmentStringsW
	2c2d2	  413  FreeEnvironmentStringsW
	2c2ec	  709  GetStringTypeW
	2c2fe	  674  GetProcessHeap
	2c310	 1504  WriteConsoleW
	2c320	  761  GetTimeZoneInformation
	2c33a	  824  HeapSize
	2c346	  822  HeapReAlloc
	2c354	 1258  SetEndOfFile
	2c374	 1088  RaiseException

 0002ba90	0002bacc 00000000 00000000 0002be20 00021000

	DLL Name: ADVAPI32.dll
	vma:  Hint/Ord Member-Name Bound-To
	2bde8	  129  ConvertStringSecurityDescriptorToSecurityDescriptorW

 0002baa4	0002bc48 00000000 00000000 0002be2e 0002117c

	DLL Name: WS2_32.dll
	vma:  Hint/Ord Member-Name Bound-To
	8000000e	   14  <none>

 0002bab8	00000000 00000000 00000000 00000000 00000000
```
<div style="page-break-after: always; visibility: hidden"> 
\pagebreak
</div>
